LDFILE="link.ld"
CCFLAG=-Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -g
CFILES=$(wildcard *.c)
COBJS=$(CFILES:.c=.o)
ASMFILES=$(wildcard *.S)
ASMOBJS=$(ASMFILES:.S=.o)
#AS = i586-elf-as 
#CC = i586-elf-gcc
#LD = i586-elf-ld

all: kernel.bin


$(COBJS): %.o: %.c
	$(CC) $(CCFLAG) -o $@ $< -m32

$(ASMOBJS): %.o: %.S
	$(AS) --gstabs+ -o $@ $< -32

kernel.elf: $(ASMOBJS) $(COBJS)
	$(LD) -T $(LDFILE) -o $@ $^ -m elf_i386

kernel.bin: kernel.elf
	objcopy -O binary $< $@

myos.iso: kernel.bin
	cp kernel.bin isodir/boot
	grub-mkrescue -o $@ isodir

.PHONY: clean
clean:
	rm *.o *.bin *.iso *.elf

test:
	echo $(COBJS)
