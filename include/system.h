#ifndef __SYSTEM_H
#define __SYSTEM_H

typedef int size_t;

/* main.c */
unsigned char *memcpy(unsigned char * dest, const unsigned char * src, int count);
unsigned char *memset(unsigned char *dest, unsigned char val, int count);
unsigned short *memsetw(unsigned short *dest, unsigned short val, int count);
int strlen(const char *str);
unsigned char inportb(unsigned short _port);
void outportb(unsigned short _port, unsigned char _data);

/*scrn.c*/
void cls();
void putch(unsigned char c);
void puts(unsigned char * str);
void settextcolor(unsigned char forecolor, unsigned char backcolor);
void init_video(void);

/* gdt.c */
void gdt_set_gate(int num, unsigned long base, unsigned long limit, unsigned char access, unsigned char gran);
void gdt_install();

/* idt.c */
void idt_set_gate(unsigned char num, unsigned long base, unsigned short sel, unsigned char flags);
void idt_install();

/* isrs.c */
/* This defines what the stack looks like after an ISR was running */
struct regs
{
    unsigned int gs, fs, es, ds; /* pushed the segs last */
    unsigned int edi, esi, ebp, esp, ebx, edx, ecx, eax; /* pushed by 'pusha' */
    unsigned int int_no, err_code; /* our 'push' and encodes do this */
    unsigned int eip, cs, eflags, userresp, ss; /* pushed by the processor automatically */
}; 
void isrs_install();

/* irq.c */
void irq_install();
void irq_install_handler(int irq, void (*handler)(struct regs *r));
void irq_uninstall_handler(int irq);

/* timer.c */
void timer_install();
void timer_wait(int ticks);
void timer_phase(int hz);

/* kb.c */
void keyboard_install();
#endif
