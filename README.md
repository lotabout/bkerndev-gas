## Description
This is a GAS clone of (bkerndev)[http://www.osdever.net/bkerndev/Docs/title.htm]. 

bkerndev is a very nice tutorial for building a brand new kernel. However, it
uses NASM as the default assembler, and a `build.bat` script to make the project.

I'm using Linux and learning GNU as (which uses the so-called AT&T syntax). So I
followed this tutorial and implements the assembly part using GAS.

If you're interested in AT&T syntax and kernel development, this could be a reference
to you.

## Requirements To Build
You should have `binutils` and `gcc` installed. If you want to take advantage of the
`Makefile`, then `make` is needed.

Additional: if you want to make an iso file from the kernel, you should have `grub`
installed.

## How to Build
1. download the repository
2. enter the working directory and run `make`
3. if you want to make an iso. run `make myos.iso`
    
## How to Run
I use `qemu`(which is a emulator) to test the kernel.

For `kernel.bin`:
    Run `qemu-system-i386 -kernel kernel.bin`

For `myos.iso`:
    Run `qemu-system-i386 -cdrom myos.iso`

## What to expect?
After run `qemu`, a `Hello World` should appear on the screen.

Then about every second, a line `One second has passed` should appear on the screen.

Finally, any keys you pressed will appear on the screen.

## License
I don't know what license of the original `bkerndev` has. So, I should say this
follows the original.
