#include <system.h>

/* You will need to code these up yourself! */
unsigned char *memcpy(unsigned char * dest, const unsigned char * src, int count)
{
    unsigned char * p_dest = dest;
    const unsigned char * p_src = src;
    while (count --)
    {
        *p_dest = *p_src;
        p_dest ++;
        p_src ++;
    }
    return dest;
}

unsigned char *memset(unsigned char *dest, unsigned char val, int count)
{
    /* Add code here to set 'count' bytes in 'dest' to 'val'. */
    unsigned char * p_dest = dest;
    while (count --)
    {
        *p_dest = val;
        p_dest ++;
    }
    return dest;
}

unsigned short *memsetw(unsigned short *dest, unsigned short val, int count)
{
    /* Same as above, but this time, we're working with a 16-bit
     * 'val' and dest pointer. Your code can be exact copy of the
     * above, provided that your local variables if any, are unsigned short*/
    unsigned short * p_dest = dest;
    while (count --)
    {
        *p_dest = val;
        p_dest ++;
    }
    return dest;
}

int strlen(const char *str)
{
    const char * p = str;
    int len = 0;
    while (*(p++))
    {
        len ++;
    }
    return len;
}

/* We will use this later on for reading from the I/O ports to get data
 * from devices such as the keyboard. We are using what is called
 * 'inline assembly' in these routines to actually do the work */
unsigned char inportb(unsigned short _port)
{
    unsigned char rv;
    __asm__ __volatile__ ("inb %1, %0"
            : "=a" (rv)
            : "dN" (_port));
    return rv;
}

/* We will use this to write I/O ports to send bytes to devices. This 
 * will be used in the next tutorial for changing the textmode cursor
 * position. Again, we use some inline assembly for the stuff that 
 * simply connot be done in C */
void outportb(unsigned short _port, unsigned char _data)
{
    asm volatile (
            "outb %1, %0"
            :
            : "dN"(_port), "a"(_data)
            );
}

/* This is a very simple main() function. All it does is sit in an 
 * infinite loop. This will be like our 'idle' loop*/
int main()
{
    gdt_install();
    idt_install();
    init_video();
    isrs_install();
    irq_install();
    timer_install();
    keyboard_install();
    __asm__ __volatile__("sti");

    puts("Hello World.\n");

    for (;;);

    return 0;
}
