#include <system.h>

/* Defines an IDT entry */
struct idt_entry
{
    unsigned short base_low;
    unsigned short selector;  /* Our kernel segment goes here! */
    unsigned char  zeros;     /* This will ALWAYS be set to 0! */
    unsigned char  flags;     
    unsigned short base_high;
} __attribute__ ((packed));

struct idt_ptr
{
    unsigned short size;
    unsigned long offset;
} __attribute__ ((packed));

/* Declare an IDE of 256 entries. Although we will only use the the
 * first 32 entries in this tutorial, the reset exists as a bit 
 * of a trap. If any undefined IDE entry is hit, it normally
 * will cause an "Unhandled Interrupt" exception. Any descriptor
 * for which the 'presence' bit is cleared(0) will generate an 
 * "Unhandled Interrupt" exception */
struct idt_entry idt[256];
struct idt_ptr idtp;

/* This exists in 'start.asm', and is used to load our IDT */
extern void idt_load();

/* Use this function to set an entry in the IDT. A lot simpler
 * than twiddling with the GDT :) */
void idt_set_gate(unsigned char num, unsigned long base, unsigned short sel, unsigned char flags)
{
    idt[num].base_low = base & 0xFFFF;
    idt[num].selector = sel;
    idt[num].zeros = 0;
    idt[num].flags = flags;
    idt[num].base_high = (base >> 16) & 0xFFFF;
}

/* Installs the IDT */
void idt_install()
{
    /* Sets the special IDT pointer up, just like in 'gdt.c' */
    idtp.size = (sizeof(struct idt_entry) * 256) - 1;
    idtp.offset = &idt;

    /* Clear out the entire IDT, initializing it to zeros */
    memset(&idt, 0, sizeof(struct idt_entry)*256);

    /* Add any new ISRs to the IDT here using idt_set_gate */

    /* Points the processor's internal register to the new IDT */
    idt_load();
}
